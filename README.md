# licensing

| reference                                                       | license      |
| --------------------------------------------------------------- | ------------ |
| https://pixabay.com/en/sunrise-space-outer-globe-world-1756274/ | CC0          |
| https://commons.wikimedia.org/wiki/File:Logo_Ferderation.svg    | CC BY-SA 4.0 |
